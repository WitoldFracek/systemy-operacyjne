package Procesorowanie;
import java.util.*;
public class ProcesComparator<T> implements Comparator<Proces> {
	
	public int compare(Proces p1, Proces p2)
	{
		int r = 0;
		if(p1.getTime() < p2.getTime())
		{
			r = -1;
		}
		if(p1.getTime() > p2.getTime())
		{
			r = 1;
		}
		return r;
	}

}
