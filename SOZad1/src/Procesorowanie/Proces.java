package Procesorowanie;

public class Proces{
	
	private int time;
	private int entrance;
	private int exit;
	private int inProgress;
	private int token;
	
	public Proces(int i, int t)
	{
		entrance=i;
		time=t;
		inProgress=t;
		token = 0;
	}
	
	
	public void tickDown()
	{
		inProgress--;
	}
	
	public boolean isDone()
	{
		return (inProgress <= 0);
	}
	
	public int waitingTime(int span)
	{
		int wait;
		double korekta;
		wait = exit - entrance;
		korekta = (double)time/(double)span;
		wait -= korekta;
		return wait+1;
	}
	
	public void giveToken(int q)
	{
		token = q;
	}
	
	
	public int getTime()
	{
		return inProgress;
	}
	
	public void setExit(int e)
	{
		exit = e;
	}
	
	public int getToken()
	{
		return token;
	}
	
	public void setToken(int tok)
	{
		token = tok;
	}
	
	public void tokenTickDown()
	{
		token--;
	}
	
	@Override
	public Proces clone()
	{
		return new Proces(entrance, time);
	}
	
	public int compareTo(Proces p)
	{
		int r = 0;
		if(inProgress < p.getTime())
		{
			r = -1;
		}
		if(inProgress > p.getTime())
		{
			r = 1;
		}
		return r;
	}
	
	public boolean czyZaglodzony(int span)
	{
		boolean zaglodzony = false;
		if(exit != 0 && !zaglodzony)
		{
			if(waitingTime(span) > 4*time)
			{
				zaglodzony = true;
			}
		}
		return zaglodzony;
	}

}
