package Procesorowanie;
import java.util.ArrayList;
import java.io.*;
public class KlasaTestowa {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		
		int liczbaZgloszonychProcesow = 1000000;
		int wyswietlCoIleProcesow = 10000;
		int czasMaksymalny = 15;	// Maksymalny czas jaki mo�e potrzebowa� proces
		
		int kwantCzasu = 5;
		double prawdopodobienstwoZgloszeniaNowegoProcesu = 0.2;	//Prawdopodobie�stwo pojawienia nowego procesu. W przypadku odczytu z pliku, aby za ka�dym razem otrzymywa� por�wnywalne wyniki jest to warto�� m�wi�ca co ile jednostek czasu pojawi si� nowy proces.
		int tick = 100;											//Mo�liwo�� ustawienia o ile zmniejsza si� czas potrzebny na wykonanie procesu przy ka�dym obrocie p�tli.
		
		String st = "D:\\HDD\\Programowanie Java\\EclipseGit\\SOZad1\\Dane_4.txt";
		Procesor proc = new Procesor(czasMaksymalny, prawdopodobienstwoZgloszeniaNowegoProcesu, kwantCzasu, wyswietlCoIleProcesow, liczbaZgloszonychProcesow, tick);
	
		//Dane_1 - liczby od 1 do 100
		//Dane_2 - liczby od 1 do 15
		//Dane_3 - liczby od 1 do 3
		//Dane_4 - liczby od 1 do 1000
		//Dane_5 - liczby od 1 do 20
		
		
		System.out.println("Program wy�wietla �rednie czasy oczekiwania dla r�nych rodzaj�w kolejek.");
		System.out.println("W poni�szym za�o�eniu ka�da kolejka w kt�rym� momencie przestaje otrzymywa� nowe zg�oszenia");
		System.out.println("wi�c wszystkie procesy ostatecznie zostan� wykonane.");
		System.out.println("Okre�lenie 'procesy zag�odzone' odnosi si� do proces�w, kt�re na przydzia� do procesora czeka�y");
		System.out.println("czteroktornie d��ej ni� wynosi� ich czas potrzebny na realizacj�.");
		System.out.println("*************************************************");
		
		proc.startFIFO(st);	//Metoda uruchamiaj�ca kolejk� typu FIFO
		
		System.out.println("*************************************************");
		
		proc.startSJF(st);	//Metoda uruchamiaj�ca kolejk� typu SJF
		
		System.out.println("*************************************************");
		
		proc.startROT(st);	//Metoda uruchamiaj�ca kolejk� typu Rotacyjnego
		
		System.out.println("*************************************************");
		
		//proc.startFIFOandSJFandROT();	//Metoda uruchamiaj�ca wszystkie trzy rodzaje kolejek w tym samym czasie dla losowych danych wej�ciowych.
		
		System.out.println("*************************************************");
		
	}

}
