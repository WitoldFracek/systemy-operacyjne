package Procesorowanie;
import java.util.ArrayList;
import java.io.*;
import java.lang.IllegalStateException;
public class Procesor {
	
	private ArrayList<Proces> array;
	
	int maxTime;
	double probability;
	int quant;
	int ileProcesow;
	int coIleWyswietl;
	int oneTickSpan;
	
	public Procesor()
	{
		array = new ArrayList<>();
		maxTime = 1000;
		probability = 0.20;
		quant = 5;
		ileProcesow = 10000;
		coIleWyswietl = 1000;
		oneTickSpan = 70;
	}
	
	public Procesor(int maxT, double prb, int q, int sp, int liczbaP, int ots)
	{
		array = new ArrayList<>();
		maxTime = maxT;
		probability = prb;
		quant = q;
		oneTickSpan = ots;
		ileProcesow = liczbaP;
		coIleWyswietl = sp;
		if(oneTickSpan < 1) throw new IllegalStateException();
	}
	
	public void startFIFO()
	{
		boolean overcount = false;
		int osCzasu = 1;
		int sumaCzasuOczekiwania = 0;
		int procesyZakonczone = 0;
		int ileProcesow = 0;
		
		while(ileProcesow < this.ileProcesow && !overcount)
		{
			if(Math.random() > (1.00-probability))
			{
				array.add(new Proces(osCzasu, (int)((Math.random()*maxTime))+1));
			}
			ileProcesow++;
			if(array.size() > 0)
			{
				Proces temp = array.get(0);
				temp.tickDown();
				if(temp.isDone())
				{
					array.remove(0);
					procesyZakonczone++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwania < 0)
					{
						overcount = true;
					}
				}
				else
				{
					array.set(0, temp);
				}
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("�redni czas oczekiwania FIFO: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			if(sumaCzasuOczekiwania < 0)
			{
				overcount = true;
			}
			osCzasu++;
		}
		while(array.size() > 0)
		{
			Proces temp = array.get(0);
			for(int i=0; i<oneTickSpan; i++)
			{
				temp.tickDown();
			}
			if(temp.isDone())
			{
				array.remove(0);
				procesyZakonczone++;
				temp.setExit(osCzasu);
				sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
				if(sumaCzasuOczekiwania < 0)
				{
					overcount = true;
				}
			}
			else
			{
				array.set(0, temp);
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("�redni czas oczekiwania FIFO: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			if(sumaCzasuOczekiwania < 0)
			{
				overcount = true;
			}
			osCzasu++;	
		}
	}
	
	public void startFIFO(String plikDanych) throws FileNotFoundException, IOException
	{
		FileReader fr = new FileReader(plikDanych);
		BufferedReader br = new BufferedReader(fr);
		array.clear();
		
		long tMax = 0;
		long procesyZaglodzone = 0;
		boolean overcount = false;
		int osCzasu = 1;
		int sumaCzasuOczekiwania = 0;
		int procesyZakonczone = 0;
		String tempus = br.readLine();
		int czas = 0;
		
		while(tempus != null)
		{
			czas = Integer.parseInt(tempus);
			if(osCzasu%(int)(1/(probability)) == 0 )
			{
				array.add(new Proces(osCzasu, czas));
				tempus = br.readLine();
			}
			if(array.size() > 0)
			{
				Proces temp = array.get(0);
				for(int i=0; i<oneTickSpan; i++)
				{
					temp.tickDown();
				}
				if(temp.isDone())
				{
					array.remove(0);
					procesyZakonczone++;
					temp.setExit(osCzasu);
					if(temp.czyZaglodzony(oneTickSpan))
					{
						procesyZaglodzone++;
					}
					if(temp.waitingTime(oneTickSpan) > tMax)
					{
						tMax = temp.waitingTime(oneTickSpan);
					}
					sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwania < 0)
					{
						overcount = true;
					}
				}
				else
				{
					array.set(0, temp);
				}
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("Po " + coIleWyswietl + " krokach");
				System.out.println("�redni czas oczekiwania FIFO: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				System.out.println("Procesy zaglodzone: " + procesyZaglodzone + "\n");
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			if(sumaCzasuOczekiwania < 0)
			{
				overcount = true;
			}
			osCzasu++;
		}
		
		while(array.size() > 0)
		{
			Proces temp = array.get(0);
			for(int i=0; i<oneTickSpan; i++)
			{
				temp.tickDown();
			}
			if(temp.isDone())
			{
				array.remove(0);
				procesyZakonczone++;
				temp.setExit(osCzasu);
				if(temp.clone().czyZaglodzony(oneTickSpan))
				{
					procesyZaglodzone++;
				}
				if(temp.waitingTime(oneTickSpan) > tMax)
				{
					tMax = temp.waitingTime(oneTickSpan);
				}
				sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
				if(sumaCzasuOczekiwania < 0)
				{
					overcount = true;
				}
			}
			else
			{
				array.set(0, temp);
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("Po " + coIleWyswietl + " krokach");
				System.out.println("�redni czas oczekiwania FIFO: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				System.out.println("Procesy zaglodzone: " + procesyZaglodzone + "\n");
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			if(sumaCzasuOczekiwania < 0)
			{
				overcount = true;
			}
			osCzasu++;	
		}
		br.close();
		System.out.println("Najd�u�szy czas oczekiwania FIFO: " + tMax);
		
	}
	
	public void startSJF()
	{
		array.clear();
		
		ProcesComparator<Proces> pc = new ProcesComparator<>();
		int osCzasu = 1;
		int sumaCzasuOczekiwania = 0;
		int procesyZakonczone = 0;
		boolean overcount = false;
		int ileProcesow = 0;
		
		while(ileProcesow < this.ileProcesow && !overcount)
		{
			if(Math.random() > (1.00-probability))
			{
				array.add(new Proces(osCzasu, (int)((Math.random()*maxTime))+1));
				array.sort(pc);
			}
			ileProcesow++;
			if(array.size() > 0)
			{
				Proces temp = array.get(0);
				temp.tickDown();
				if(temp.isDone())
				{
					array.remove(0);
					procesyZakonczone++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
					array.sort(pc);
					if(sumaCzasuOczekiwania < 0)
					{
						overcount = true;
					}
				}
				else
				{
					array.set(0, temp);
					array.sort(pc);
				}
				
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("�redni czas oczekiwania SJF: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			osCzasu++;
		}
		while(array.size() > 0)
		{
			Proces temp = array.get(0);
			for(int i=0; i<oneTickSpan; i++)
			{
				temp.tickDown();
			}
			if(temp.isDone())
			{
				array.remove(0);
				procesyZakonczone++;
				temp.setExit(osCzasu);
				sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
				if(sumaCzasuOczekiwania < 0)
				{
					overcount = true;
				}
			}
			else
			{
				array.set(0, temp);
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("�redni czas oczekiwania SJF: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			if(sumaCzasuOczekiwania < 0)
			{
				overcount = true;
			}
			osCzasu++;	
		}
	}
	
	public void startSJF(String plikDanych) throws FileNotFoundException, IOException
	{
		array.clear();
		ProcesComparator<Proces> pc = new ProcesComparator<>();
		FileReader fr = new FileReader(plikDanych);
		BufferedReader br = new BufferedReader(fr);
		int dana;
		int osCzasu = 1;
		int sumaCzasuOczekiwania = 0;
		int procesyZakonczone = 0;
		boolean overcount = false;
		String tempus = br.readLine();
		long procesyZaglodzone = 0;
		long tMax = 0;
		
		while(tempus != null)
		{
			dana = Integer.parseInt(tempus);
			if(osCzasu%(int)(1/(probability)) == 0 )
			{
				array.add(new Proces(osCzasu, dana));
				array.sort(pc);
				tempus = br.readLine();
			}
			if(array.size() > 0)
			{
				Proces temp = array.get(0);
				for(int i=0; i<oneTickSpan; i++)
				{
					temp.tickDown();
				}
				if(temp.isDone())
				{
					array.remove(0);
					procesyZakonczone++;
					temp.setExit(osCzasu);
					if(temp.czyZaglodzony(oneTickSpan))
					{
						procesyZaglodzone++;
					}
					if(temp.waitingTime(oneTickSpan) > tMax)
					{
						tMax = temp.waitingTime(oneTickSpan);
					}
					sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
					array.sort(pc);
					if(sumaCzasuOczekiwania < 0)
					{
						overcount = true;
					}
				}
				else
				{
					array.set(0, temp);
					array.sort(pc);
				}
				
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("Po " + coIleWyswietl + " krokach");
				System.out.println("�redni czas oczekiwania SJF: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				System.out.println("Procesy zaglodzone: " + procesyZaglodzone + "\n");
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			osCzasu++;
		}
		while(array.size() > 0)
		{
			Proces temp = array.get(0);
			for(int i=0; i<oneTickSpan; i++)
			{
				temp.tickDown();
			}
			if(temp.isDone())
			{
				array.remove(0);
				procesyZakonczone++;
				temp.setExit(osCzasu);
				if(temp.czyZaglodzony(oneTickSpan))
				{
					procesyZaglodzone++;
				}
				if(temp.waitingTime(oneTickSpan) > tMax)
				{
					tMax = temp.waitingTime(oneTickSpan);
				}
				sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
				if(sumaCzasuOczekiwania < 0)
				{
					overcount = true;
				}
			}
			else
			{
				array.set(0, temp);
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("Po " + coIleWyswietl + " krokach");
				System.out.println("�redni czas oczekiwania SJF: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				System.out.println("Procesy zaglodzone: " + procesyZaglodzone + "\n");
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			if(sumaCzasuOczekiwania < 0)
			{
				overcount = true;
			}
			osCzasu++;	
		}
		fr.close();
		System.out.println("Najd�u�szy czas oczekiwania SJF: " + tMax);
	}
	
	public void startFIFOandSJF()  // Metoda testowa, nie zalecane u�ywanie
	{
		ProcesComparator<Proces> pc = new ProcesComparator<>();
		ArrayList<Proces> afifo = new ArrayList<>();
		ArrayList<Proces> asjf = new ArrayList<>();
		int osCzasu = 1;
		int sumaCzasuOczekiwaniaFIFO = 0;
		int procesyZakonczoneFIFO = 0;
		int sumaCzasuOczekiwaniaSJF = 0;
		int procesyZakonczoneSJF = 0;
		boolean overcount = false;
		int ileProcesow = 0;
		
		while(ileProcesow < this.ileProcesow && !overcount)
		{
			if(Math.random() > (1.00-probability))
			{
				Proces temp = new Proces(osCzasu, (int)((Math.random()*maxTime))+1);
				afifo.add(temp.clone());
				asjf.add(temp.clone());
				asjf.sort(pc);
			}
			ileProcesow++;
			if(afifo.size() > 0)
			{
				Proces temp = afifo.get(0);
				temp.tickDown();
				if(temp.isDone())
				{
					afifo.remove(0);
					procesyZakonczoneFIFO++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwaniaFIFO += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwaniaFIFO < 0)
					{
						overcount = true;
					}
				}
				else
				{
					afifo.set(0, temp);
				}
			}
			if(asjf.size() > 0)
			{
				Proces temp = asjf.get(0);
				temp.tickDown();
				if(temp.isDone())
				{
					asjf.remove(0);
					asjf.sort(pc);
					procesyZakonczoneSJF++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwaniaSJF += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwaniaSJF < 0)
					{
						overcount = true;
					}
				}
				else
				{
					asjf.set(0, temp);
					asjf.sort(pc);
				}
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.print("�redni czas oczekiwania FIFO: " + liczSrednia(sumaCzasuOczekiwaniaFIFO, procesyZakonczoneFIFO) + " | ");
				System.out.println("�redni czas oczekiwania SJF: " + liczSrednia(sumaCzasuOczekiwaniaSJF, procesyZakonczoneSJF));
			}
			osCzasu++;
		}
	}
	
	public Proces findToken(ArrayList<Proces> ar)
	{
		Proces temp = null;
		for(Proces p: ar)
		{
			if(p.getToken() > 0)
			{
				temp = p;
			}
		}
		return temp;
	}
	
	public void startROT()
	{
		boolean overcount = false;
		int osCzasu = 1;
		int sumaCzasuOczekiwania = 0;
		int procesyZakonczone = 0;
		int ileProcesow = 0;
		
		while(ileProcesow < this.ileProcesow && !overcount)
		{
			if(Math.random() > (1.00-probability))
			{
				array.add(new Proces(osCzasu, (int)((Math.random()*maxTime))+1));
			}
			ileProcesow++;
			if(array.size() > 0)
			{
				Proces temp = findToken(array);
				if(temp == null)
				{
					temp = array.get(0);
					temp.setToken(quant);
				}
				temp.tickDown();
				temp.tokenTickDown();
				if(temp.isDone())
				{
					array.remove(temp);
					procesyZakonczone++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwania < 0)
					{
						overcount = true;
					}
				}
				else
				{
					if(temp.getToken() <= 0)
					{
						temp.setToken(0);
						Proces naKoniecKolejki = temp;
						array.remove(temp);
						if(array.size() > 0)
						{
							temp = array.get(0);
							temp.setToken(quant);
						}
						array.add(naKoniecKolejki);
					}
				}
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("�redni czas oczekiwania: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			osCzasu++;
		}
	}
	
	public void startROT(String plikDanych) throws FileNotFoundException, IOException
	{
		FileReader fr = new FileReader(plikDanych);
		BufferedReader br = new BufferedReader(fr);
		array.clear();
		
		long tMax = 0;
		long procesyZaglodzone = 0;
		boolean overcount = false;
		int osCzasu = 1;
		int sumaCzasuOczekiwania = 0;
		int procesyZakonczone = 0;
		int czas;
		String tempus = br.readLine();
		while(tempus != null)
		{
			czas = Integer.parseInt(tempus);
			if(osCzasu%(int)(1/(probability)) == 0 )
			{
				array.add(new Proces(osCzasu, czas));
				tempus = br.readLine();
			}
			if(array.size() > 0)
			{
				Proces temp = findToken(array);
				if(temp == null)
				{
					temp = array.get(0);
					temp.setToken(quant);
				}
				for(int i=0; i<oneTickSpan; i++)
				{
					temp.tickDown();
				}
				temp.tokenTickDown();
				if(temp.isDone())
				{
					array.remove(temp);
					procesyZakonczone++;
					temp.setExit(osCzasu);
					if(temp.czyZaglodzony(oneTickSpan))
					{
						procesyZaglodzone++;
					}
					if(temp.waitingTime(oneTickSpan) > tMax)
					{
						tMax = temp.waitingTime(oneTickSpan);
					}
					sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwania < 0)
					{
						overcount = true;
					}
				}
				else
				{
					if(temp.getToken() <= 0)
					{
						temp.setToken(0);
						Proces naKoniecKolejki = temp;
						array.remove(temp);
						if(array.size() > 0)
						{
							temp = array.get(0);
							temp.setToken(quant);
						}
						array.add(naKoniecKolejki);
					}
				}
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("Po " + coIleWyswietl + " krokach");
				System.out.println("�redni czas oczekiwania ROT: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				System.out.println("Procesy zaglodzone: " + procesyZaglodzone + "\n");
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			osCzasu++;
		}
		while(array.size() > 0)
		{
			Proces temp = array.get(0);
			for(int i=0; i<oneTickSpan; i++)
			{
				temp.tickDown();
			}
			if(temp.isDone())
			{
				array.remove(0);
				procesyZakonczone++;
				temp.setExit(osCzasu);
				if(temp.czyZaglodzony(oneTickSpan))
				{
					procesyZaglodzone++;
				}
				if(temp.waitingTime(oneTickSpan) > tMax)
				{
					tMax = temp.waitingTime(oneTickSpan);
				}
				sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
				if(sumaCzasuOczekiwania < 0)
				{
					overcount = true;
				}
			}
			else
			{
				array.set(0, temp);
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.println("Po " + coIleWyswietl + " krokach");
				System.out.println("�redni czas oczekiwania ROT: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				System.out.println("Procesy zaglodzone: " + procesyZaglodzone + "\n");
				sumaCzasuOczekiwania = 0;
				procesyZakonczone = 0;
			}
			if(sumaCzasuOczekiwania < 0)
			{
				overcount = true;
			}
			osCzasu++;	
		}
		fr.close();
		System.out.println("Najd�u�szy czas oczekiwania ROT: " + tMax);
	}
	
	public void startFIFOandSJFandROT()
	{
		ProcesComparator<Proces> pc = new ProcesComparator<>();
		ArrayList<Proces> afifo = new ArrayList<>();
		ArrayList<Proces> asjf = new ArrayList<>();
		ArrayList<Proces> arot = new ArrayList<>();
		int osCzasu = 1;
		int sumaCzasuOczekiwaniaFIFO = 0;
		int procesyZakonczoneFIFO = 0;
		int sumaCzasuOczekiwaniaSJF = 0;
		int procesyZakonczoneSJF = 0;
		int sumaCzasuOczekiwaniaROT = 0;
		int procesyZakonczoneROT = 0;
		boolean overcount = false;
		int ileProcesow = 0;
		
		while(ileProcesow < this.ileProcesow && !overcount)
		{
			if(Math.random() > (1.00-probability))
			{
				Proces temp = new Proces(osCzasu, (int)((Math.random()*maxTime))+1);
				afifo.add(temp.clone());
				asjf.add(temp.clone());
				asjf.sort(pc);
				arot.add(temp.clone());
			}
			ileProcesow++;
			if(afifo.size() > 0)
			{
				Proces temp = afifo.get(0);
				for(int i=0; i < oneTickSpan; i++)
				{
					temp.tickDown();
				}
				if(temp.isDone())
				{
					afifo.remove(0);
					procesyZakonczoneFIFO++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwaniaFIFO += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwaniaFIFO < 0)
					{
						overcount = true;
					}
				}
				else
				{
					afifo.set(0, temp);
				}
			}
			if(asjf.size() > 0)
			{
				Proces temp = asjf.get(0);
				for(int i=0; i < oneTickSpan; i++)
				{
					temp.tickDown();
				}
				if(temp.isDone())
				{
					asjf.remove(0);
					asjf.sort(pc);
					procesyZakonczoneSJF++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwaniaSJF += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwaniaSJF < 0)
					{
						overcount = true;
					}
				}
				else
				{
					asjf.set(0, temp);
					asjf.sort(pc);
				}
			}
			if(arot.size() > 0)
			{
				Proces temp = findToken(arot);
				if(temp == null)
				{
					temp = arot.get(0);
					temp.setToken(quant);
				}
				for(int i=0; i < oneTickSpan; i++)
				{
					temp.tickDown();
				}
				temp.tokenTickDown();
				if(temp.isDone())
				{
					arot.remove(temp);
					procesyZakonczoneROT++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwaniaROT += temp.waitingTime(oneTickSpan);
					if(sumaCzasuOczekiwaniaROT < 0)
					{
						overcount = true;
					}
				}
				else
				{
					if(temp.getToken() <= 0)
					{
						temp.setToken(0);
						Proces naKoniecKolejki = temp;
						arot.remove(temp);
						if(arot.size() > 0)
						{
							temp = arot.get(0);
							temp.setToken(quant);
						}
						arot.add(naKoniecKolejki);
					}
				}
			}
			if(osCzasu%coIleWyswietl == 0)
			{
				System.out.print("FIFO: " + liczSrednia(sumaCzasuOczekiwaniaFIFO, procesyZakonczoneFIFO) + " | ");
				System.out.print("SJF: " + liczSrednia(sumaCzasuOczekiwaniaSJF, procesyZakonczoneSJF) + " | ");
				System.out.println("ROT: " + liczSrednia(sumaCzasuOczekiwaniaROT, procesyZakonczoneROT));
				sumaCzasuOczekiwaniaFIFO = 0;
				procesyZakonczoneFIFO = 0;
				sumaCzasuOczekiwaniaSJF = 0;
				procesyZakonczoneSJF = 0;
				sumaCzasuOczekiwaniaROT = 0;
				procesyZakonczoneROT = 0;
			}
			osCzasu++;
		}
	}
	
	public void startTest()  //Metoda pomocnicza, niezalecane uruchamianie
	{
		int osCzasu = 1;
		long sumaCzasuOczekiwania = 0;
		int procesyZakonczone = 0;
		
		while(true)
		{
			array.add(new Proces(osCzasu, 2));
			if(array.size() > 0)
			{
				Proces temp = array.get(0);
				temp.tickDown();
				if(temp.isDone())
				{
					array.remove(0);
					procesyZakonczone++;
					temp.setExit(osCzasu);
					sumaCzasuOczekiwania += temp.waitingTime(oneTickSpan);
				}
				else
				{
					array.set(0, temp);
				}
				if(osCzasu%100000 == 0)
				{
					System.out.println("�redni czas oczekiwania: " + liczSrednia(sumaCzasuOczekiwania, procesyZakonczone));
				}
				osCzasu++;
			}
		}
	}
	
	public double liczSrednia(long suma, int ilosc)
	{
		double srednia;
		if(ilosc == 0)
		{
			srednia = -1;
		}
		else
		{
			srednia = (double)suma/(double)ilosc;
		}
		return srednia;
	}

}
